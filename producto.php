<?php
include 'config.php';
class ProductoDB{
    
    function AddProductos($name,$detail,$price,$img)
    {
        try{
            $category=1;
            $iduser=$this->getidUser();
            $db = getDB();
            $query = $db->prepare("INSERT INTO producto(iduser,idcat,nombre,descripcion,precio,imagen) VALUES (:iduser,:idcat,:nombre,:descripcion,:precio,:imagen)");
            $query->bindParam("iduser", $iduser ,PDO::PARAM_INT) ;
            $query->bindParam("idcat", $category ,PDO::PARAM_INT) ;
            $query->bindParam('nombre', $name,PDO::PARAM_STR) ; //Password encryption
            $query->bindParam("descripcion", $detail,PDO::PARAM_STR) ;
            $query->bindParam("precio", $price,PDO::PARAM_STR);
            $query->bindParam("imagen", $img,PDO::PARAM_STR);
            $query->execute();
        }catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    }

    public function getidUser()
    {
        $db = getDB();
        $query2 = $db->prepare("SELECT * FROM  usuario WHERE idreg=:idusuario"); 
        $query2->bindParam("idusuario", $_SESSION['id'],PDO::PARAM_INT) ;
        $query2->execute();
        $data2=$query2->fetch(PDO::FETCH_OBJ);
        echo $data2->id;
        return $data2->id;
    }
//https://gitlab.com/techhome/clasificadosbolivia
    function getProductos(){
        try{
            $db = getDB();
            $sql = "SELECT * FROM producto;";
            $stmt = $db->prepare($sql);
            $stmt->execute(); 

            $arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $arr;
        }catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    }

    function addComentario($idProducto,$comentario)
    {
        $iduser = $_SESSION["iduser"];
        # code...
    }
}


?>