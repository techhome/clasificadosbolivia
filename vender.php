<?php
    include 'producto.php';
    define ('SITE_ROOT', realpath(dirname(__FILE__)));
    if(!empty($_POST["submit"])){
        $target_path =  "uploads/";
        $target_path = $target_path .  $_FILES['uploadfile']['name'];
        if(move_uploaded_file($_FILES['uploadfile']['tmp_name'], $target_path)) {
            //echo "El archivo ". basename( $_FILES['uploadfile']['name']). " ha sido subido";
          $product = new ProductoDB();
          $product->AddProductos($_POST["name"],$_POST["detail"],$_POST['price'],$target_path);
        } else{
            echo "Ha ocurrido un error, trate de nuevo!";
        }
    }
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <title>Clasificados Bolivia</title>
    <style>
     .nav-item > button{
        margin-left:10px;
        margin-right:10px;
     }
     .camera{
       margin-right:8px;
     }
     .card-footer-color{
        background-color:#66BB6A;
        color: #ffff !important;
        font-weight:bold;
     }
     .card-product{
      -webkit-box-shadow: -1px 4px 48px -5px rgba(0,0,0,0.75);
      -moz-box-shadow: -1px 4px 48px -5px rgba(0,0,0,0.75);
      box-shadow: -1px 4px 48px -5px rgba(0,0,0,0.75);
     }
    </style>
  </head>
  <body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Clasificados Bolivia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <!-- <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li> -->
    </ul>
    <ul class="navbar-nav">
    <?php if(empty($_SESSION['id'])){  ?>
      <li class="nav-item">
       <button class="btn btn-success" data-toggle="modal" data-target="#loginmodal">Ingresar</button>
      </li>
      <li class="nav-item">
       <button class="btn btn-success" data-toggle="modal" data-target="#registermodal">Registrarse</button>
      </li>
      <?php }else{ ?>
        <li class="nav-item">
        <a class="navbar-brand" href="#">Hola <?php echo $_SESSION["id"] ?></a>
      </li>
      <li class="nav-item">
       <a href="logout.php" class="btn btn-success" role="button">logout</a>
      </li>
        <?php } ?>
      <li class="nav-item">
       <button class="btn btn-success">Ayuda</button>
      </li>
      <li class="nav-item">
        <button type="button" href="vender.php" class="btn btn-outline-success"><i class="fas fa-camera-retro camera"></i>Vender!!</button>
      </li>
    </ul>
  </div>
</nav>
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="http://gusanito.com/v5img/img/Banners_Home_Nuevo.png" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="http://gusanito.com/v5img/img/Banners_Home_Nuevo.png" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="http://gusanito.com/v5img/img/Banners_Home_Nuevo.png" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>  
    </header>

    <form enctype="multipart/form-data" action="" method="POST">
    <input name="uploadfile" type="file"/>
    <input name="name" type="text" placeholder="name" />
    <input name="price" type="text" placeholder="price" />
    <input name="detail" type="text" placeholder="detail" />
    <input type="submit" name="submit" value="Subir archivo" />
    </form>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
  </body>
</html>