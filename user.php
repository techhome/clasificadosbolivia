<?php
include 'config.php';
class UserDB{
    public function Login($email,$password){
        try{
            $db = getDB();
            $hash_password= hash('sha256', $password); 
            $query = $db->prepare("SELECT id FROM  register WHERE (username=:usernameEmail or email=:usernameEmail) AND contra=:hash_password"); 
            $query->bindParam("usernameEmail", $email,PDO::PARAM_STR) ;
            $query->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
            $query->execute();
            $count=$query->rowCount();
            $data=$query->fetch(PDO::FETCH_OBJ);
            
            if($count){
                $_SESSION['id']=$data->id; 
                $query2 = $db->prepare("SELECT * FROM  usuario WHERE idreg=:idusuario"); 
                $query2->bindParam("idusuario", $_SESSION['id'],PDO::PARAM_INT) ;
                $query2->execute();
                $persona=$query2->fetch(PDO::FETCH_OBJ);
                $_SESSION['nombre']=$persona->nombre;
                $_SESSION['iduser']=$persona->id; 
                $db = null;
                return true;
            }
            else{
                $db = null;
                return false;
            } 
        }

        catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    }

    public function getidUser()
    {
        $query2 = $db->prepare("SELECT * FROM  usuario WHERE idreg=:idusuario"); 
        $query2->bindParam("idusuario", $_SESSION['id'],PDO::PARAM_INT) ;
        $query2->execute();
        $data2=$query2->fetch(PDO::FETCH_OBJ);
        return $data2->id;
    }

    public function RegistrarUsuario($nombre,$email,$password,$username)
    {
        $db = getDB();
        try{
            $db = getDB();
            $st = $db->prepare("SELECT id FROM register WHERE username=:username OR email=:email"); 
            $st->bindParam("username", $username,PDO::PARAM_STR);
            $st->bindParam("email", $email,PDO::PARAM_STR);
            $st->execute();
            $count=$st->rowCount();
            if($count<1)
            {
                //aqui preparamos el query para que insertemos en la tabla register
                $query = $db->prepare("INSERT INTO register(username,contra,email) VALUES (:username,:hash_password,:email)");
                $query->bindParam("username", $username,PDO::PARAM_STR) ;
                $hash_password= hash('sha256', $password); //Password encryption
                $query->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
                $query->bindParam("email", $email,PDO::PARAM_STR);
                $query->execute();
                $id=$db->lastInsertId();
                $_SESSION['id']=$id;
                // aqui preparamos el query para que insertemos en la tabla usuario con el id de registro que obtenemos
                $query2 = $db->prepare("INSERT INTO usuario(email,nombre,idreg) VALUES (:email,:nombre,:idreg)");
                $query2->bindParam("email", $email,PDO::PARAM_STR);
                $query2->bindParam("nombre", $nombre,PDO::PARAM_STR);
                $query2->bindParam("idreg", $id,PDO::PARAM_INT);
                $query2->execute();

                $query3 = $db->prepare("SELECT id,nombre FROM  usuario WHERE idreg=:idusuario"); 
                $query3->bindParam("idusuario", $_SESSION['id'],PDO::PARAM_INT) ;
                $query3->execute();
                $data2=$query3->fetch(PDO::FETCH_OBJ);
                $_SESSION['nombre']=$data2->nombre;
                $_SESSION['iduser']=$data2->id;


                $db = null;

                return true;
            }else{
                $db = null;
                return false;
            }
                
        } 
        catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    }

    function getProductos(){
        try{
            $db = getDB();
            $sql = "SELECT * FROM producto;";
            $stmt = $db->prepare($sql);
            $stmt->execute(); 

            $arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $arr;
        }catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    }
    function addComentario($idProducto,$comentario)
    {
        $iduser = $_SESSION["iduser"];
        $db = getDB();
        $query2 = $db->prepare("INSERT INTO comentario(iduser,idpro,detalle) VALUES (:iduser,:idpro,:detalle)");
        $query2->bindParam("iduser", $iduser,PDO::PARAM_INT);
        $query2->bindParam("idpro", $idProducto,PDO::PARAM_INT);
        $query2->bindParam("detalle", $comentario,PDO::PARAM_STR);
        $query2->execute();
    }


    function getComentarios($idproducto){
        try{
            $db = getDB();
            $stmt = $db->prepare("SELECT * FROM comentario Where idpro=:idproducto");
            $stmt->bindParam("idproducto", $idproducto,PDO::PARAM_INT);
            $stmt->execute(); 
//http://localhost/clasificadosbolivia/detalleproducto.php?idproduct=8
            $arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $arr;
        }catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    }

    function getProducto($idproducto)
    {
        $db = getDB();
        $query3 = $db->prepare("SELECT * FROM  producto WHERE id=:idproducto"); 
        $query3->bindParam("idproducto", $idproducto, PDO::PARAM_INT) ;
        $query3->execute();
        $data2=$query3->fetch(PDO::FETCH_OBJ);
        return $data2;
    }
}

?>
