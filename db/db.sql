create table Register(
    id int primary key AUTO_INCREMENT,
    username varchar(100),
    email varchar(100),
    contra varchar(100)
)
create table Usuario
(
    id int primary key AUTO_INCREMENT,
    nombre varchar(100),
    telefono varchar(100),
    direccion varchar(100),
    email varchar(100),
    idreg int,
    foreign key (idreg) references Register(id)
)
create table Categoria
(
    id int primary key AUTO_INCREMENT,
    nombre varchar(100)
)
create table Producto(
    id int primary key AUTO_INCREMENT,
    iduser int,
    idcat int,
    nombre VARCHAR(100),
    descripcion Text,
    precio VARCHAR(10),
    moneda VARCHAR(10),
    imagen VARCHAR(100),
    foreign key (iduser) REFERENCES Usuario(id),
    foreign key (idcat) REFERENCES Categoria(id)
)
create table Comentario(
    id int primary key AUTO_INCREMENT,
    iduser int,
    idpro int,
    detalle varchar(100),
    foreign key (iduser) REFERENCES Usuario(id),
    foreign key (idpro) references Producto(id)
)
create table Imagen(
    id int primary key AUTO_INCREMENT,
    img VARCHAR(100),
    idpro int,
    foreign key (idpro) REFERENCES Producto(id)
)


