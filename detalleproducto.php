<?php
/*
  ALTER TABLE register
  ADD UNIQUE (username,email);
*/
  include 'user.php';
  if(!empty($_POST["submit"])){
    $usuario = new UserDB();
    if($_POST['submit']=="login"){
      // ejecutar el login
        $usuario->Login($_POST['emaillogin'],$_POST['passlogin']);
    }else{
      if($_POST['submit']=="register"){
        $usuario->RegistrarUsuario($_POST['nombre'],$_POST['email'],$_POST['password'],$_POST['username']);
      }else{
        if($_POST['submit']=="comentario"){
            $usuario->addComentario($_GET['idproduct'],$_POST['comentario']);
        }
      }
    }
  }
  $usuario = new UserDB();
  $producto = $usuario->getProducto($_GET['idproduct'])
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <title>Clasificados Bolivia</title>
    <style>
     .nav-item > button{
        margin-left:10px;
        margin-right:10px;
     }
     .camera{
       margin-right:8px;
     }
     .card-footer-color{
        background-color:#66BB6A;
        color: #ffff !important;
        font-weight:bold;
     }
     .card-product{
      -webkit-box-shadow: -1px 4px 48px -5px rgba(0,0,0,0.75);
      -moz-box-shadow: -1px 4px 48px -5px rgba(0,0,0,0.75);
      box-shadow: -1px 4px 48px -5px rgba(0,0,0,0.75);
     }
    </style>
  </head>
  <body>
    <header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Clasificados Bolivia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <!-- <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li> -->
    </ul>
    <ul class="navbar-nav">
    <?php if(empty($_SESSION['id'])){  ?>
      <li class="nav-item">
       <button class="btn btn-success" data-toggle="modal" data-target="#loginmodal">Ingresar</button>
      </li>
      <li class="nav-item">
       <button class="btn btn-success" data-toggle="modal" data-target="#registermodal">Registrarse</button>
      </li>
      <?php }else{ ?>
        <li class="nav-item">
        <a class="navbar-brand" href="#">Hola <?php echo $_SESSION["nombre"] ?></a>
      </li>
      <li class="nav-item">
       <a href="logout.php" class="btn btn-success" role="button">logout</a>
      </li>
        <?php } ?>
      <li class="nav-item">
       <button class="btn btn-success">Ayuda</button>
      </li>
      <li class="nav-item">
        <button type="button" onclick = "location='vender.php'" class="btn btn-outline-success"><i class="fas fa-camera-retro camera"></i>Vender!!</button>
      </li>
    </ul>
  </div>
</nav>
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="http://gusanito.com/v5img/img/Banners_Home_Nuevo.png" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="http://gusanito.com/v5img/img/Banners_Home_Nuevo.png" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="http://gusanito.com/v5img/img/Banners_Home_Nuevo.png" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>  
    </header>
<div class="container">
<div class=col-lg-12>
  <div class="row">
    <div class="col-lg-6">
      <img class="img-fluid" src="<?php echo $producto->imagen; ?>">
    </div>
    <div class="col-lg-6">
          <h1><?php echo $producto->nombre; ?></h1>
      <p><?php echo $producto->descripcion; ?></p>
    </div>
  </div>
  </div>
   <form action="" method="post">
       
        <label for="exampleInputPassword1">Comentario</label>
        <div class="form-group">
            
            <textarea name="comentario" class="form-control"></textarea> 
        </div>
        <button type="submit" class="btn btn-primary"  name="submit" value="comentario">Submit</button>
    </form>

     <?php $usuario = new UserDB();
            $arr=$usuario->getComentarios($_GET["idproduct"]);
            foreach ($arr as $row) { ?>
                <h1> <?php echo $row['detalle']; ?></h1>
            <?php } ?>
</div>
     
  <!-- Modal Login -->
  <div class="modal fade" id="loginmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ingresar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="" method="POST">
          <div class="modal-body">
          
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" name="emaillogin" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" name="passlogin" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" name="submit" value="login">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>


  <!-- Modal Register -->
  <div class="modal fade" id="registermodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Registrases</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="" method="POST">
          <div class="modal-body">
          <div class="form-group">
              <label for="exampleInputEmail1">Nombre</label>
              <input type="text" name="nombre" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Introducir Nombre">
              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
             <div class="form-group">
              <label for="exampleInputEmail1">Username</label>
              <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Introducir su Username">
              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Introducir Correo">
              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" name="submit" value="register">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js"></script>
  </body>
</html>